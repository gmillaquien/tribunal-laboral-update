

# Consultas hacia poder Judicial

Este script  realiza  las consultas hacia la pagina https://laboral.pjud.cl/ consultando cada juzgado por cada  personaa ingresada en el archivo excel.

# Si la página del poder judicial se cae, el script fallará y deberá reiniciar la ejecución.



Requerimientos Mínimos

-------------------------

- Instalar Python 2.7   [Descargar ](https://www.python.org/downloads/release/python-2717/),
el proyecto fue desarrollado en esta version de python, ya que existen limitaciones en las librerìas que se utilizan.

- Verificar que las variables de entorno se encuentre "C:\Python27;C:\Python27\Scripts;". Esto se puede verificar en mi pc.

![PATH ](/img/Path.png)

---------------------------------------

Se debe  descargar el driver correspondiente a nuestro navegador, por defecto este script utiliza el driver de google chrome.

Para revisar la versión, se debe ir a ayuda, acerca de y verificar su version del navegador.

![Acerca](/img/acercade.png)  


![Version Chrome](/img/version.png)  

De esta página descargamos el driver correspondiente y validámos que  sea correspondiente a la versión instalada en nuestro equipo.

 [Descargar  Driver Chrome ](https://chromedriver.chromium.org/downloads)


![Descarga Driver ](/img/descargaDriver.png)  

Copiamos el driver en la siguiente ubicación de nuestro PC, C:\Python27\Scripts.


![Copiar  Driver ](/img/driverexplorador.png)  







-------------------------------------

## Cambios del Script

Selección del Juzgado.

El robot puede recorrer todo el listado de los juzgados, pero también puede recorrer ciertos juzgados como se ve a continuación.


![Total Juzgados](/img/listadodejuzgados.png)  

Para seleccionar  la zona de un juzgado específico, se debe cambiar dentro de  linea 124 el nombre de la variable @{TotalJuzgado} por la del listado presente a continuaciòn,ejemplo @{TotalRancagua}. Se guarda y se ejecuta.

@{TotalArica}     1333

@{TotalIquique}    1334 1500 6

@{TotalAntofagasta}    13 1335 1357 14 1501 26

@{TotalCopiapo}    1336    27 29 34 36 37    386

@{TotalLaSerena}    13347 46 47 48 49 50 51 52 53

@{TotalValparaiso}    101    102    103    1338 1358 660 83 84 85 86 87 88 89    90 94 96 97 98 99
@{TotalRancagua}    111 113 114 115 1150    1151    116 117 119 1339

@{TotalTalca}     126 127 132 133    1340 1341 135 136 138 139 140 141

@{TotalChillan}    1342 147    149    150    151    152

@{TotalConcepcion}    1152 1343    1359    157 158    159 160 187 188 189 190 191 192 193 194 195 196 385

@{TotalTemuco}    1344 204    206 207 208 209 210 211 212 213 214 215 216 946 947

@{TotalValdivia}    1345 1360 222 223 224 225 226 227

@{TotalPuertoMontt}    1013 1346 1361 238 240 241 243 244 245 659

@{TotalCoyhaique}    1362 248 249 250 996

@{TotalPuntaArenas}    1347 1502 257 258

@{TotalSantiago}    1348 1349 387 1351 1352 1363 373 374 375 377 378 388



![Buscar casos](/img/buscarjuzgado.png)  


 




-----------------------------------------




## Ejecución del Script


Descargamos el proyecto y lo descomprimimos.
Se escribe la palabra cmd en la barra de direcciones de windows para poder abrir una terminal cmd directamente y ejecutar los siguientes comandos.

![CMD](/img/cmdcarpeta.png)  

Ejecutar el comando   ``` .\env\Scripts\activate ``` .

![ENV](/img/env.png)  


Luego instalamos las dependencias.


Ejecutamos el comando y esperamos    ``` pip install -r requirements.txt    ``` . 


Para  que el script pueda guardar sin problemas en el archivo Excel, se debe reemplazar en la ruta  Proyecto/env/Lib/site-packages/ExcelLibrary, el contenido del rar que se adjunta  en los archivos del proyecto.
-------------------------------------

![Version Chrome](/img/conexcel.png)  

-------------------------------------



En caso de que presente un error  realizar el siguiente copiado de archivo.

Para  que el script pueda guardar sin problemas en el archivo Excel, se debe reemplazar en la ruta  C:\Python27\Lib\site-packages\ExcelLibrary, el contenido del rar que se adjunta  en los archivos del proyecto.


![Version Chrome](/img/contenido.png)  


Para iniciar el script ejecutamos lo siguiente  ``` python ejecutame.py ``` .

 

![Ejecutar](/img/ejecutar.png)  

Deberiamos poder visualizar como se  abre Chrome y realiza la búsqueda.

![Consultar](/img/consultasitioweb.png)  


Se verifica la información que se desea consultar, la cual debe estar en un excel con el nombre **Nombres.xls** dentro de la carpeta resultado.

![Contenido Resultado](/img/contenidoresultado.png)  

 
![Formato Excel](/img/excel.png)  




El  resultado se encontrara en la carpeta resultado.

![Resultado](/img/resultado.png)  